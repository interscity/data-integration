**Projeto em desenvolvimento**

**Faça o clone do projeto**
```
	git clone https://gitlab.com/interscity/data-integration.git
```


**Acesse o diretório do projeto**
```
	cd data-integration
```

**Inicie os containers do kafka**
```
    cd infrastructure\kafka
	docker-compose up -d
```

**Inicie os containers do consumidor**
```
    cd ../../data-ingestion/consumer
    docker-compose up -d --build --remove-orphans
```


**Inicie os containers do produtor de dados em batch (API)**
```
    cd ../../data-ingestion/producer
    docker-compose up -d --build --remove-orphans
```


**Rode o comando abaixo para verificar se os serviços producer_app e consumer_app estão ativos**
```
	docker stats
```

**Inicie a aplicação consumidora de dados em batch**
```
	docker exec -it consumer_app /bin/bash
    export TOPIC=batch-data
    go mod tidy
    go run src/consumer.go
```



**Inicie a API**
```
    docker exec -it producer_app /bin/bash
    export TOPIC=batch-data
    go mod tidy
    go run src/producer.go
```


**Inicie os containers do MongoDB**
```
    cd infrastructure/Mongo/
    docker-compose up -d --build --remove-orphans
```

**Inicie os containers do Metadata Management**
```
    cd metadata-management
    docker-compose up -d --build --remove-orphans
```


**Inicie os containers do API Gateway**
```
    cd infrastructure/kong
    docker-compose up -d --build --remove-orphans
```

**Inicie os containers do Data Query**
```
    docker-compose up -d --build --remove-orphans
```

**Teste**
```
    curl --location --request POST 'localhost:8393/api/v1/data' --header 'Content-Type: application/json' --data-raw '{ "text":"Message 1" }'
    curl -v -F 'myFile=@data-article.csv' localhost:8393/api/v1/data
```
	
