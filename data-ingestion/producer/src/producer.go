package main

import (
	"fmt"
	"log"
	"os"
	"github.com/Shopify/sarama"
	"github.com/gofiber/fiber/v2"
	"errors"
	"encoding/json"
	"io/ioutil"
	"strings"
)

// Comment struct
type Comment struct {
	Text string `form:"text" json:"text"`
}
const ACTIVE = "1"

func main() {

	app := fiber.New()
	api := app.Group("/api/v1") // /api

	api.Post("/data", createData)

	app.Listen(":8393")

}

func ConnectProducer(brokersUrl []string) (sarama.SyncProducer, error) {

	config := sarama.NewConfig()
	config.Producer.Return.Successes = true
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Retry.Max = 5

	conn, err := sarama.NewSyncProducer(brokersUrl, config)
	if err != nil {
		return nil, err
	}

	return conn, nil
}

func PushDataToQueue(topic string, message []byte) error {

	brokersUrl := []string{"host.docker.internal:9092"}
	producer, err := ConnectProducer(brokersUrl)
	if err != nil {
		return err
	}

	defer producer.Close()

	msg := &sarama.ProducerMessage{
		Topic: topic,
		Value: sarama.StringEncoder(message),
	}

	partition, offset, err := producer.SendMessage(msg)
	if err != nil {
		return err
	}

	fmt.Printf("Message is stored in topic(%s)/partition(%d)/offset(%d)/message(%s)\n", topic, partition, offset, message)

	return nil
}

func createDataJson(c *fiber.Ctx) error {
	cmtInBytes := c.Body()
	if !(json.Valid(cmtInBytes)) {
		err := errors.New("Invalid json")
		log.Println(err)

		c.Status(400).JSON(&fiber.Map{
			"success": false,
			"message": err,
		})
		
		return err
	}

	err := PushDataToQueue(os.Getenv("TOPIC"), cmtInBytes)

	if err != nil {
		log.Println(err)
		c.Status(500).JSON(&fiber.Map{
			"success": false,
			"message": "Error PushDataToQueue",
		})
		
		return err
	}

	err = c.JSON(&fiber.Map{
		"success": true,
		"message": "Data pushed successfully",
	})

	if err != nil {
		log.Println(err)
		c.Status(500).JSON(&fiber.Map{
			"success": false,
			"message": err,
		})
		return err
	}
	return err
}

func createDataCSV(c *fiber.Ctx) error {
	fileH, err := c.FormFile("myFile")
	if err != nil {
		log.Println(err)
		c.Status(400).JSON(&fiber.Map{
			"success": false,
			"message": err,
		})
	}
	file, err := fileH.Open()
	if err != nil {
		return err
	}
	fileBytes, err := ioutil.ReadAll(file)
	file.Close()
	if err != nil {
		return err
	}

	err = PushDataToQueue(os.Getenv("TOPIC"), fileBytes)

	if err != nil {
		log.Println(err)
		c.Status(500).JSON(&fiber.Map{
			"success": false,
			"message": "Error PushDataToQueue",
		})
		
		return err
	}

	err = c.JSON(&fiber.Map{
		"success": true,
		"message": "Data pushed successfully",
	})

	if err != nil {
		log.Println(err)
		c.Status(500).JSON(&fiber.Map{
			"success": false,
			"message": err,
		})
		
		return err
	}
	return err
}

func createData(c *fiber.Ctx) error {
	if ((c.Get("Content-Type") == "application/json") &&  os.Getenv("ACCEPT_JSON") == ACTIVE) {
		return createDataJson(c)
	}
	if (strings.Contains(c.Get("Content-Type"), "multipart/form-data")  &&  os.Getenv("ACCEPT_CSV") == ACTIVE) {
		return createDataCSV(c)
	}
	c.Status(400).JSON(&fiber.Map{
		"success": false,
		"message": "invalid Content-Type",
	})
	return errors.New("Invalid Content-Type")
}