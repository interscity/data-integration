
import time
import json
from json import load
from kafka import KafkaConsumer
from datetime import datetime
import base64
import pyhdfs

ip = '147.182.161.38'	
kafkaport = '9092'	
hdfsport = '10015'
brokers='localhost:9092'
topic='datasource'
sleep_time=300
offset='latest'
tmpdir = '/opt/code/tmp'

consumer = KafkaConsumer ( 
    'datasource', 
     bootstrap_servers = ['localhost: 9092'], 
     auto_offset_reset = 'earliest', 
     enable_auto_commit = True, 
     group_id = 'my-grupo', 
     value_deserializer = lambda x: load ('utf- 8 '))

#consumer = KafkaConsumer(bootstrap_servers=brokers)
consumer.subscribe(['datasource'])

oldprice_dict = {}
while(True):
    for message in consumer:
        print(message)
        d=json.loads(message.value)
        fileData = base64.b64decode(d['content'])
        filename = d['filename']
        username = d['username']
        aDate = d['date']
        directory = '/' + username + '/' + aDate + '/'
        fs.mkdirs(directory)
        print(filename)
        tmpfile = tmpdir + directory + filename
        if not os.path.exists(tmpdir + directory):
            os.makedirs(tmpdir + directory)
        f = open(tmpfile, "w")
        f.write(fileData)
        f.close()

        file_manager = FileManager()
        file_manager.file_upload("localhost:10015", "hdfs", tmpfile, directory + filename)

    time.sleep(sleep_time)

