module data/ingestion/consumer

go 1.14

require (
	github.com/Shopify/sarama v1.27.2
	github.com/klauspost/compress v1.11.6 // indirect
	golang.org/x/net v0.0.0-20201016165138-7b1cca2348c0 // indirect
)
